/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.evaluation;

/**
 *
 * @author gerito
 */
public class EvaluationTimesMerging {
    private int numberEntries;
    private long totalTime;
    private long inTime;
    private long outTime;
    private long memoryStimationTime;

    public EvaluationTimesMerging() {
    }

    public EvaluationTimesMerging(int numberEntries, long totalTime, long inTime, long outTime, long memoryStimationTime) {
        this.numberEntries = numberEntries;
        this.totalTime = totalTime;
        this.inTime = inTime;
        this.outTime = outTime;
        this.memoryStimationTime = memoryStimationTime;
    }

    public int getNumberEntries() {
        return numberEntries;
    }

    public void setNumberEntries(int numberEntries) {
        this.numberEntries = numberEntries;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public long getInTime() {
        return inTime;
    }

    public void setInTime(long inTime) {
        this.inTime = inTime;
    }

    public long getOutTime() {
        return outTime;
    }

    public void setOutTime(long outTime) {
        this.outTime = outTime;
    }

    public long getMemoryStimationTime() {
        return memoryStimationTime;
    }

    public void setMemoryStimationTime(long memoryStimationTime) {
        this.memoryStimationTime = memoryStimationTime;
    }
    
    
}
