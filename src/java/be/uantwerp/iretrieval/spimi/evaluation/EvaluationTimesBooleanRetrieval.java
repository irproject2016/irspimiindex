/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.evaluation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.json.simple.JSONArray;

/**
 *
 * @author gerito
 */
public class EvaluationTimesBooleanRetrieval {
    private long totalTime;
    private long inTime;
    private long memoryStimationTime;
    private int numberOfReults;
    private JSONArray result = new JSONArray();
    private String query;
    private boolean compression;
    private LinkedHashMap<String, ArrayList> processedQuery;

    public EvaluationTimesBooleanRetrieval() {
    }

    public LinkedHashMap<String, ArrayList> getProcessedQuery() {
        return processedQuery;
    }

    public void setProcessedQuery(LinkedHashMap<String, ArrayList> processedQuery) {
        this.processedQuery = processedQuery;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public boolean isCompression() {
        return compression;
    }

    public void setCompression(boolean compression) {
        this.compression = compression;
    }

    public JSONArray getResult() {
        return result;
    }

    public void setResult(JSONArray result) {
        this.result = result;
    }

    public int getNumberOfReults() {
        return numberOfReults;
    }

    public void setNumberOfReults(int numberOfReults) {
        this.numberOfReults = numberOfReults;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public long getInTime() {
        return inTime;
    }

    public void setInTime(long inTime) {
        this.inTime = inTime;
    }

    public long getMemoryStimationTime() {
        return memoryStimationTime;
    }

    public void setMemoryStimationTime(long memoryStimationTime) {
        this.memoryStimationTime = memoryStimationTime;
    }
    
    
}
