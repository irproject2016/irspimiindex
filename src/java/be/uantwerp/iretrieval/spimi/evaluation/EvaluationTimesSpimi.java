/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.evaluation;

import org.json.simple.JSONArray;

/**
 *
 * @author gerito
 */
public class EvaluationTimesSpimi {

    private JSONArray result;
    private long totalTime;
    private long inTime;
    private long outTime;
    private long memoryStimationTime;
    private int numberOfResultingBlocks;

    public EvaluationTimesSpimi() {
    }

    public EvaluationTimesSpimi(JSONArray result, long totalTime, long inTime, long outTime, long memoryStimationTime) {
        this.result = result;
        this.totalTime = totalTime;
        this.inTime = inTime;
        this.outTime = outTime;
        this.memoryStimationTime = memoryStimationTime;
    }

    public int getNumberOfResultingBlocks() {
        return numberOfResultingBlocks;
    }

    public void setNumberOfResultingBlocks(int numberOfResultingBlocks) {
        this.numberOfResultingBlocks = numberOfResultingBlocks;
    }

    public long getMemoryStimationTime() {
        return memoryStimationTime;
    }

    public void setMemoryStimationTime(long memoryStimationTime) {
        this.memoryStimationTime = memoryStimationTime;
    }

    public JSONArray getResult() {
        return result;
    }

    public void setResult(JSONArray result) {
        this.result = result;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public long getInTime() {
        return inTime;
    }

    public void setInTime(long inTime) {
        this.inTime = inTime;
    }

    public long getOutTime() {
        return outTime;
    }

    public void setOutTime(long outTime) {
        this.outTime = outTime;
    }

}
