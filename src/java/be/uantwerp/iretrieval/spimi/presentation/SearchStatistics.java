package be.uantwerp.iretrieval.spimi.presentation;

//import be.uantwerp.iretrieval.spimi.json.Parameter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

@WebServlet(name = "SearchStatistics", urlPatterns = {"/statistics"})
public class SearchStatistics extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/statistics.jsp");
        dispatcher.forward(request, response);
    }

}
