package be.uantwerp.iretrieval.spimi.presentation;

import be.uantwerp.iretrieval.spimi.dataTransfer.Constants;
import be.uantwerp.iretrieval.spimi.dataTransfer.Document;
import java.io.IOException;
import be.uantwerp.iretrieval.spimi.utility.FileUtility;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import be.uantwerp.iretrieval.spimi.dataTransfer.JsonInputOutput;
import be.uantwerp.iretrieval.spimi.evaluation.EvaluationTimesCompression;
import be.uantwerp.iretrieval.spimi.evaluation.EvaluationTimesMerging;
import be.uantwerp.iretrieval.spimi.evaluation.EvaluationTimesSpimi;
import be.uantwerp.iretrieval.spimi.logic.MergeSPIMIInvertedIndex;
import be.uantwerp.iretrieval.spimi.logic.PostingListCompressor;
import be.uantwerp.iretrieval.spimi.utility.PreProcessing;
import java.nio.charset.Charset;
import java.util.List;
import be.uantwerp.iretrieval.spimi.logic.SPIMIInvertedIndex;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

@WebServlet(name = "GenerateFilesServlet", urlPatterns = {"/processes"})
public class GenerateFilesServlet extends HttpServlet {

    private JsonInputOutput jsonIO = new JsonInputOutput();
    private SPIMIInvertedIndex spimiII = new SPIMIInvertedIndex();
    private MergeSPIMIInvertedIndex mergeSI = new MergeSPIMIInvertedIndex();
    private Constants constant = new Constants();
    private PostingListCompressor plCompress = new PostingListCompressor();
    private final int initialNumber = 1000000;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("views/processes.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileNotFoundException {
        String type = request.getParameter("type");
        switch (type) {
            case "0":
                PreProcessing process = new PreProcessing(constant.GET_PATHSTOPWORDS());
                List<String> listFiles = FileUtility.getFilesFolder(constant.GET_FOLDERPATHROOTDOCUMENTS());
                System.out.println("Pre-process of the files");
                for (int i = 0; i < listFiles.size(); i++) {
                    String doc = FileUtility.readFile(constant.GET_FOLDERPATHROOTDOCUMENTS() + listFiles.get(i), Charset.defaultCharset());
                    String docTransformed = process.documentTransformation(doc);
                    FileUtility.writeDocument(constant.GET_FOLDERPATHRESULTDOCUMENTS() + listFiles.get(i), docTransformed);
                }
                break;
            case "1":
                List<String> listFiles2 = FileUtility.getFilesFolder(constant.GET_FOLDERPATHRESULTDOCUMENTS());
                System.out.println("creation of the document with the json information");
                JSONArray objList = new JSONArray();
                for (int i = initialNumber; i < (listFiles2.size() + initialNumber); i++) {
                    Document document = new Document(Long.parseLong(String.valueOf(i)), listFiles2.get(i - initialNumber), constant.GET_FOLDERPATHRESULTDOCUMENTS() + listFiles2.get(i - initialNumber));
                    JSONObject obj = jsonIO.createJsonObj(document);
                    objList.add(obj);
                }
                FileUtility.writeDocument(constant.GET_NAMELISTDOCUMENTS(), objList.toJSONString());
                break;
            case "2":
                EvaluationTimesSpimi evalSpimi = spimiII.InitalizeIndexConstruction();
                EvaluationTimesMerging evalMerge = new EvaluationTimesMerging();
                try {
                    evalMerge= mergeSI.InitalizeMerge(evalSpimi.getResult());
                } catch (ParseException ex) {
                    Logger.getLogger(GenerateFilesServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                EvaluationTimesCompression evalCompression = new EvaluationTimesCompression();
                try {
                    evalCompression = plCompress.compressor(evalMerge.getNumberEntries());
                } catch (ParseException ex) {
                    Logger.getLogger(GenerateFilesServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.getSession().setAttribute("evalSpimi", evalSpimi);
                request.getSession().setAttribute("evalMerge", evalMerge);
                request.getSession().setAttribute("evalCompression", evalCompression);
                break;
            default:
                break;
        }
        response.sendRedirect("http://localhost:8080/irspimiindex/processes");

    }

}
