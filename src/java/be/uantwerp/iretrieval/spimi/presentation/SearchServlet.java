package be.uantwerp.iretrieval.spimi.presentation;

//import be.uantwerp.iretrieval.spimi.json.Parameter;
import be.uantwerp.iretrieval.spimi.dataTransfer.Constants;
import be.uantwerp.iretrieval.spimi.dataTransfer.Document;
import be.uantwerp.iretrieval.spimi.evaluation.EvaluationTimesBooleanRetrieval;
import be.uantwerp.iretrieval.spimi.logic.BooleanSearch;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import be.uantwerp.iretrieval.spimi.utility.PreProcessing;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@WebServlet(name = "SearchServlet", urlPatterns = {"/SearchServlet"})
public class SearchServlet extends HttpServlet {

    private BooleanSearch booleanSearch = new BooleanSearch();
    public static String PATHSTOPWORDS = "C:\\Users\\gerito\\Documents\\SpimiIndex\\stopwords.txt";
    private final Constants constant = new Constants();
    private HashMap<Long, Document> documents = new HashMap<>();
    PreProcessing textAnalyser = new PreProcessing(PATHSTOPWORDS);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/index.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String query = request.getParameter("searchText");
        String chkBoxContro = request.getParameter("toggle");
        boolean compression = false;
        if (null == chkBoxContro) {
            compression = false;
        } else {
            compression = true;
        }
        
        System.out.println("query before analyzer: "+query);
        LinkedHashMap<String, ArrayList> processedQuery = textAnalyser.QueryAnalyser(query);
        System.out.println("query: "+processedQuery);
        EvaluationTimesBooleanRetrieval resultSet = new EvaluationTimesBooleanRetrieval();
        try {
            resultSet = booleanSearch.InitSearch(processedQuery, compression);
            resultSet.setQuery(query);
            resultSet.setProcessedQuery(processedQuery);

        } catch (ParseException ex) {
            Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.getSession().setAttribute("compression", compression);
        try {
            createDocuments();
        } catch (ParseException ex) {
            Logger.getLogger(SearchServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.getSession().setAttribute("queryTerm", query);
        request.getSession().setAttribute("documentResults", transformListDocuments(resultSet.getResult()));
        request.getSession().setAttribute("timeQuery", String.valueOf(resultSet.getTotalTime()/1000.0));
        request.getSession().setAttribute("numberResults", resultSet.getNumberOfReults());
        if (request.getSession().getAttribute("resultList")==null){
            List<EvaluationTimesBooleanRetrieval> resultListEval = new ArrayList<>();
            resultListEval.add(resultSet);
            request.getSession().setAttribute("resultList", resultListEval);
        }else{
            List<EvaluationTimesBooleanRetrieval> resultListEval = (List<EvaluationTimesBooleanRetrieval>) request.getSession().getAttribute("resultList");
            resultListEval.add(resultSet);
            System.out.println("evaluation retrivals");
            for (int i=0;i<resultListEval.size();i++){
                System.out.println(resultListEval.get(i).getResult());
            }
            request.getSession().setAttribute("resultList", resultListEval);
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/search.jsp");
        dispatcher.forward(request, response);
    }

    private List<Document> transformListDocuments(JSONArray json) {
        List<Document> resultOfDocuments = new ArrayList<>();
        Iterator itr = json.iterator();
        while (itr.hasNext()) {
            Long docId = (long) itr.next();
            resultOfDocuments.add(documents.get(docId));
        }
        return resultOfDocuments;
    }

    private void createDocuments() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(constant.GET_NAMELISTDOCUMENTS()));
        JSONArray jsonDocList = (JSONArray) obj;
        for (int i = 0; i < jsonDocList.size(); i++) {
            JSONObject jsonObj = (JSONObject) jsonDocList.get(i);
            Document document = new Document((long) jsonObj.get("documentId"), (String) jsonObj.get("documentName"), (String) jsonObj.get("documentURL"));

            documents.put(document.getDocumentId(), document);
        }

    }

}
