package be.uantwerp.iretrieval.spimi.dataTransfer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Constants {
    private JSONObject jsonObj = new JSONObject();
    private Object obj = new Object();
    private static final String ROOT = "C:\\Users\\gerito\\Documents\\SpimiIndex\\fileHandler.json";
    /*the name of the json which will have the document id/location pair*/
    private final static String NAMELISTDOCUMENTS = "documents.json";
    /*the name of the file which will have the index in json*/
    private final static String INVERTEDINDEXJSON = "spimiIndexBlocks\\spimiIndex";
    // list of inverted index blocks
    private final static String SPIMIINDEXBLOCKS = "spimiIndexBlocks";
    /*list of stop words file name*/
    private final static String STOPWORDS = "stopwords.txt";
    private final static String SUPERBLOCKS = "superBlocks\\superBlock";
    private final static String SPIMIINDEXFINALFOLDER = "spimiIndexFinal\\";
    private final static String SPIMIINDEXFINAL = "spimiIndexFinal\\spimiIndexFinal";
    private final static String FINALINDEXCOMPRESSED = "spimiIndexFinalCompressed\\finalIndexCompressed";
    private final static String FINALINDEXCOMPRESSEDFOLDER = "spimiIndexFinalCompressed\\";
    private int DELTAMAX;
    private final JSONParser parser = new JSONParser();
    public Constants(){
        try{
            obj = parser.parse(new FileReader(ROOT));
            jsonObj = (JSONObject) obj;
        } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
    }
    
    public String GET_FOLDERPATHROOTDOCUMENTS(){
        /*The location of the files on disk without any processes*/
        return  (String) jsonObj.get("FOLDERPATHROOTDOCUMENTS");
    }
    public String GET_FOLDERPATHRESULTDOCUMENTS(){
        /*The location of the result of the preprocessing part on disk*/
        return (String) jsonObj.get("FOLDERPATHRESULTDOCUMENTS");
    }
    public String GET_FOLDERPATHDOCUMENTS(){
        /*General folder holing relevant files */
        return (String) jsonObj.get("FOLDERPATHDOCUMENTS");
    } 
    public String GET_PATHSTOPWORDS(){
        /*the path of the stop words to do the processing*/
        return (String) (String) jsonObj.get("FOLDERPATHDOCUMENTS") + STOPWORDS;
    }
    public String GET_NAMELISTDOCUMENTS(){
        return (String) (String) jsonObj.get("FOLDERPATHDOCUMENTS") + NAMELISTDOCUMENTS;
    }
    public String GET_INVERTEDINDEXJSON(){
        return (String) (String) jsonObj.get("FOLDERPATHDOCUMENTS") + INVERTEDINDEXJSON;
    }
    public String GET_SPIMIINDEXBLOCKS(){
        return (String) (String) jsonObj.get("FOLDERPATHDOCUMENTS") + SPIMIINDEXBLOCKS;
    }
    public String GET_SUPERBLOCKS(){
        return (String) (String) jsonObj.get("FOLDERPATHDOCUMENTS") + SUPERBLOCKS;
    }
    
    public int GET_DELTAMAX(){
        return this.DELTAMAX;
    }
    public void SET_DELTAMAX(int delta){
        this.DELTAMAX = delta;
    }
    public String GET_SPIMIINDEXFINAL(){
        return (String) jsonObj.get("FOLDERPATHDOCUMENTS") + SPIMIINDEXFINAL;
    }
    public String GET_SPIMIINDEXFINALFOLDER(){
        return (String) jsonObj.get("FOLDERPATHDOCUMENTS") + SPIMIINDEXFINALFOLDER;
    }
     public String GET_FINALINDEXCOMPRESSED(){
        return (String) jsonObj.get("FOLDERPATHDOCUMENTS") + FINALINDEXCOMPRESSED;
    }
    public String GET_FINALINDEXCOMPRESSEDfOLDER(){
        return (String) jsonObj.get("FOLDERPATHDOCUMENTS") + FINALINDEXCOMPRESSEDFOLDER;
    }
    
}
