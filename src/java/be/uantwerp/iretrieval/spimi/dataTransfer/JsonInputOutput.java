/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.dataTransfer;

import be.uantwerp.iretrieval.spimi.utility.CustomArrayList;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JsonInputOutput{

    public JsonInputOutput() {
    }
    
    public JSONObject createJsonObj(Document doc){
        JSONObject obj = new JSONObject();
        obj.put("documentId", doc.getDocumentId());
        obj.put("documentName", doc.getDocumentName());
        obj.put("documentURL", doc.getDocumentURL());
        
        return obj;
    }
    
    public JSONArray getJsonMapping (AbstractMap<String, CustomArrayList<Long>> dictionary){
        JSONArray jsonObjList = new JSONArray();
        
        Set set = dictionary.entrySet();
        Iterator i = set.iterator();

        while(i.hasNext()) {
           Map.Entry me = (Map.Entry)i.next();
           JSONObject jsonObj = new JSONObject();
           jsonObj.put(me.getKey(), me.getValue()); 
           jsonObjList.add(jsonObj);
        }
        
        return jsonObjList;
    }
    
}
