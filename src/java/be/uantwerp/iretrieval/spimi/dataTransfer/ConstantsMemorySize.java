/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.dataTransfer;

/**
 *
 * @author gerito
 */
public abstract class ConstantsMemorySize {
    private static final int SIZEOFBYTE = 8;
    private static final int SIZEOFINT = 32;
    private static final int SIZEOFLONG = 64;
    private static final int SIZEOFCHAR = 16;
    private static final int MAXIMUMSIZEMEMORY = 83886080;//equivalent to 10mb
    private static final int SIZEOFREFERENCEARRAY = 12;
    private static final int SIZEOREFERENCE = 32;
    private static final int SIZEOFHEADERS = 64;
    private static final int SIZEOFALIGNMENT = 32;
    private static final int SIZEOFBLOCK = 65536; //Size of the block in bytes equivalent to 64Kb

    public static int getSIZEOFINT() {
        return SIZEOFINT;
    }
    public static int getSIZEOFBYTE() {
        return SIZEOFBYTE;
    }

    public static int getSIZEOFLONG() {
        return SIZEOFLONG;
    }

    public static int getSIZEOFCHAR() {
        return SIZEOFCHAR;
    }

    public static int getMAXIMUMSIZEMEMORY() {
        return MAXIMUMSIZEMEMORY;
    }

    public static int getSIZEOFREFERENCEARRAY() {
        return SIZEOFREFERENCEARRAY;
    }

    public static int getSIZEOREFERENCE() {
        return SIZEOREFERENCE;
    }

    public static int getSIZEOFHEADERS() {
        return SIZEOFHEADERS;
    }

    public static int getSIZEOFALIGNMENT() {
        return SIZEOFALIGNMENT;
    }

    public static int getSIZEOFBLOCK() {
        return SIZEOFBLOCK;
    }        
        
}
