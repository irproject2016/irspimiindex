/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.dataTransfer;

/**
 *
 * @author gerito
 */
public class Document {
    
    public Long documentId;
    public String documentName;
    public String documentURL;

    public Document(Long documentId, String documentName, String documentURL) {
        this.documentId = documentId;
        this.documentName = documentName;
        this.documentURL = documentURL;
    }

    public Document() {
    }

    public Long getDocumentId() {
        return documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public String getDocumentURL() {
        return documentURL;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public void setDocumentURL(String documentURL) {
        this.documentURL = documentURL;
    }
    
    
}
