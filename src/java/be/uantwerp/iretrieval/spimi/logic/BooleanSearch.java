/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.logic;

import be.uantwerp.iretrieval.spimi.dataTransfer.Constants;
import be.uantwerp.iretrieval.spimi.dataTransfer.ConstantsMemorySize;
import be.uantwerp.iretrieval.spimi.evaluation.EvaluationTimesBooleanRetrieval;
import be.uantwerp.iretrieval.spimi.utility.FileUtility;
import be.uantwerp.iretrieval.spimi.utility.MemorySizeStimation;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author AddisBibish
 */
public class BooleanSearch {

    private Constants constant = new Constants();
    private PostingListCompressor encoder = new PostingListCompressor();
    private JSONParser parser = new JSONParser();
    private MemorySizeStimation memoryEstm = new MemorySizeStimation();
    private int entriesSize = 0;
    private int currentBlock = 0;
    private long timeMeasureSearch = 0;
    private long timeMeasureIn = 0;
    private long timeMeasureMemoryStimation = 0;
    private int numberOfResults = 0;

    public BooleanSearch() {
    }

    public EvaluationTimesBooleanRetrieval InitSearch(LinkedHashMap<String, ArrayList> queryData, boolean compress) throws IOException, ParseException {
        long initialTime = System.currentTimeMillis();
        EvaluationTimesBooleanRetrieval evalSearch = new EvaluationTimesBooleanRetrieval();
        LinkedHashMap<String, JSONArray> resultSet = new LinkedHashMap();
        ArrayList<String> queryTerms = queryData.get("queryList");
        System.out.println(queryTerms.toString());
        ArrayList<String> booleanOps = queryData.get("booleanOp");
        ArrayList<String> foundTerms = new ArrayList();
        JSONArray result = new JSONArray();
        int entriesSize = 0;
        int currentBlock = 0;
        List<String> blockList = null;
        if (compress == true) {
            blockList = FileUtility.getFilesFolder(constant.GET_FINALINDEXCOMPRESSEDfOLDER());
        } else {
            blockList = FileUtility.getFilesFolder(constant.GET_SPIMIINDEXFINALFOLDER());
        }
        if (blockList.size() > 0) {
            JSONArray uploadedBlocks = new JSONArray();
            while (currentBlock < blockList.size()) {
                if (queryTerms.size() == foundTerms.size())//MINIMIZE DISK I/O
                {
                    break;
                }
                File block = null;
                entriesSize = 0;
                while (entriesSize <= ConstantsMemorySize.getMAXIMUMSIZEMEMORY() && currentBlock < blockList.size()) {//simulate memory full
                    if (compress == true) {
                        long initialInTime = System.currentTimeMillis();
                        block = new File(constant.GET_FINALINDEXCOMPRESSEDfOLDER() + blockList.get(currentBlock));
                        timeMeasureIn = timeMeasureIn + (System.currentTimeMillis() - initialInTime);
                    } else {
                        long initialInTime = System.currentTimeMillis();
                        block = new File(constant.GET_SPIMIINDEXFINALFOLDER() + blockList.get(currentBlock));
                        timeMeasureIn = timeMeasureIn + (System.currentTimeMillis() - initialInTime);
                    }
                    currentBlock++;
                    Object object = parser.parse(new FileReader(block));
                    JSONArray jsonBlock = compress ? encoder.Decompressor((JSONArray) object) : (JSONArray) object;
                    JSONArray entryPl = null;
                    String entryKey = null;
                    for (int k = 0; k < jsonBlock.size(); k++) {//memory esitmation for new entries
                        JSONObject entry = (JSONObject) jsonBlock.get(k);
                        Set keySet2 = entry.keySet();
                        for (Object key : keySet2) {
                            entryKey = (String) key;
                            entryPl = (JSONArray) entry.get(key);
                        }
                        long initialMemStimationTime = System.currentTimeMillis();
                        entriesSize += memoryEstm.getSizeOfUploadEntry(entryPl.size(), entryKey)
                                + memoryEstm.getSizeOfArrayAvg();
                        timeMeasureMemoryStimation = timeMeasureMemoryStimation + (System.currentTimeMillis() - initialMemStimationTime);
                    }
                    uploadedBlocks.addAll(jsonBlock);

                }
                System.out.println("*************MEMORY FULL***********");
                JSONArray entryPl = null;
                String entryKey = null;
                for (int k = 0; k < queryTerms.size(); k++) { //foreach term NOT FOUND look up in current block
                    if (!foundTerms.contains(queryTerms.get(k))) {//if current term is alrady found no need to look up in current block
                        for (int j = 0; j < uploadedBlocks.size(); j++) {
                            JSONObject entry = (JSONObject) uploadedBlocks.get(j);
                            Set keySet2 = entry.keySet();
                            for (Object key : keySet2) {
                                entryKey = (String) key;
                                entryPl = (JSONArray) entry.get(key);
                            }
                            if (queryTerms.get(k).equals(entryKey)) {//System.out.print("FOUND "+ queryTerms.get(k) +" " +blockList.get(currentBlock)+ " "+entryKey);
                                resultSet.put(entryKey, entryPl);
                                foundTerms.add(entryKey);
                            }
                        }
                    }
                }
            }
        }
        if (resultSet.size() > 0) {
            result = BooleanRetrieval(OrderResultSet(resultSet, queryData.get("queryList")), booleanOps, queryData.get("queryList"));
            System.out.println("result " + result.toJSONString());
        }
        timeMeasureSearch = System.currentTimeMillis() - initialTime;
        numberOfResults = result.size();
        evalSearch.setTotalTime(timeMeasureSearch);
        evalSearch.setInTime(timeMeasureIn);
        evalSearch.setMemoryStimationTime(timeMeasureMemoryStimation);
        evalSearch.setNumberOfReults(numberOfResults);  
        evalSearch.setCompression(compress);
        evalSearch.setResult(result);
        return evalSearch;
    }

    public LinkedHashMap<String, JSONArray> OrderResultSet(LinkedHashMap<String, JSONArray> result, ArrayList<String> queryTerms) {
        LinkedHashMap<String, JSONArray> resultSet = new LinkedHashMap();
        JSONArray emptyArray = new JSONArray();
        for (String term : queryTerms) {
            if (!result.keySet().contains(term)) {
                resultSet.put("", emptyArray);
            }
            Set set = result.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                if (((String) me.getKey()).equals(term)) {
                    resultSet.put((String) me.getKey(), (JSONArray) me.getValue());
                }
            }
        }
        return resultSet;
    }

    public JSONArray BooleanRetrieval(LinkedHashMap<String, JSONArray> query, ArrayList<String> booleanOps, ArrayList<String> keyterms) {
        JSONArray resultSet = new JSONArray();
        JSONArray keywords = new JSONArray();
        JSONArray temp = new JSONArray();
        ArrayList<JSONArray> postingLists = new ArrayList();
        for (String key : keyterms) {
            Set set = query.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                if (me.getKey().equals(key)) {
                    keywords.add(key);
                    System.out.println("preprocess " + (String) me.getKey());
                    postingLists.add((JSONArray) me.getValue());
                    System.out.println(((JSONArray) me.getValue()).toJSONString());
                }
            }
        }
        if (!keywords.isEmpty()) {
            if (keywords.size() > 2) {
                int j = 0;
                System.out.println(" keywords size" + keywords.size() + "  boolopers size " + booleanOps.size());
                for (int x = 0; x < keywords.size(); x++) {
                    j = (x == 0) ? x : x - 1;
                    System.out.println("x " + x + "  j " + j);
                    switch (booleanOps.get(j)) {
                        case "AND":
                            if (j == 0) {
                                temp.addAll(ProcessAND(addPostingLists(postingLists.get(x), postingLists.get(++x))));
                            } else {
                                JSONArray result = new JSONArray();
                                result.addAll(ProcessAND(addPostingLists(temp, postingLists.get(x))));
                                temp.clear();
                                temp.addAll(result);
                            }
                            break;
                        case "OR":
                            if (j == 0) {
                                temp.addAll(ProcessOR(addPostingLists(postingLists.get(x), postingLists.get(++x))));
                            } else {
                                JSONArray result = new JSONArray();
                                result.addAll(ProcessOR(addPostingLists(temp, postingLists.get(x))));
                                temp.clear();
                                temp.addAll(result);
                            }
                            break;
                        case "NOT":
                            if (j == 0) {
                                temp.addAll(ProcessNOT(addPostingLists(postingLists.get(x), postingLists.get(++x))));
                            } else {
                                JSONArray result = new JSONArray();
                                result.addAll(ProcessNOT(addPostingLists(temp, postingLists.get(x))));
                                temp.clear();
                                temp.addAll(result);
                            }
                            break;
                        default:
                            break;
                    }
                }
                resultSet.addAll(temp);
            } else if (keywords.size() == 2) {
                switch (booleanOps.get(0)) {
                    case "AND":
                        resultSet.addAll(ProcessAND(addPostingLists(postingLists.get(0), postingLists.get(1))));
                        break;
                    case "OR":
                        resultSet.addAll(ProcessOR(addPostingLists(postingLists.get(0), postingLists.get(1))));
                        break;
                    case "NOT":
                        resultSet.addAll(ProcessNOT(addPostingLists(postingLists.get(0), postingLists.get(1))));
                        break;
                    default:
                        break;
                }
            } else {
                resultSet.addAll(postingLists.get(0));
            }
        }

        return resultSet;
    }

    public ArrayList<JSONArray> addPostingLists(JSONArray pl1, JSONArray pl2) {
        ArrayList<JSONArray> pLists = new ArrayList();
        pLists.add(pl1);
        pLists.add(pl2);
        return pLists;
    }

    public JSONArray ProcessAND(ArrayList<JSONArray> postingLists) {
        JSONArray resultSet = new JSONArray();
        if (!postingLists.get(0).isEmpty()) {
            Collections.sort(postingLists, new Comparator<ArrayList>() {//sort postinList arrays based on size
                public int compare(ArrayList a1, ArrayList a2) {
                    return a1.size() - a2.size();
                }
            });
            int i = 0;
            int j = 0;
            while (i < postingLists.get(0).size() && j < postingLists.get(1).size()) {// b/c first array is smaller in size
                if ((long) postingLists.get(0).get(i) == (long) postingLists.get(1).get(j)) {
                    resultSet.add((long) postingLists.get(0).get(i));
                    i++;
                    j++;
                } else if ((long) postingLists.get(0).get(i) > (long) postingLists.get(1).get(j)) {
                    j++;
                } else {
                    i++;
                }
            }
        }
        return resultSet;
    }

    public JSONArray ProcessOR(ArrayList<JSONArray> postingLists) {
        JSONArray resultSet = new JSONArray();
        System.out.println(postingLists.get(0).toJSONString());
        System.out.println(postingLists.get(1).toJSONString());
        if (!postingLists.get(0).isEmpty()) {
            int i = 0;
            int j = 0;
            while (i < postingLists.get(0).size() && j < postingLists.get(1).size()) {
                if ((long) postingLists.get(0).get(i) == (long) postingLists.get(1).get(j)) {
                    resultSet.add((long) postingLists.get(0).get(i));
                    i++;
                    j++;
                } else if ((long) postingLists.get(0).get(i) > (long) postingLists.get(1).get(j)) {
                    resultSet.add((long) postingLists.get(1).get(j));
                    j++;
                } else {
                    resultSet.add((long) postingLists.get(0).get(i));
                    i++;
                }
            }
        } else {
            resultSet.addAll(postingLists.get(1));
        }

        return resultSet;
    }

    public JSONArray ProcessNOT(ArrayList<JSONArray> postingLists) {
        JSONArray resultSet = new JSONArray();
        if (!postingLists.get(0).isEmpty()) {
            int i = 0;
            int j = 0;
            while (i < postingLists.get(0).size() && j < postingLists.get(1).size()) {
                if ((long) postingLists.get(0).get(i) == (long) postingLists.get(1).get(j)) {
                    i++;
                    j++;
                } else if ((long) postingLists.get(0).get(i) > (long) postingLists.get(1).get(j)) {
                    j++;
                } else {
                    resultSet.add((long) postingLists.get(0).get(i));
                    i++;
                }
            }
        }

        return resultSet;
    }

}
