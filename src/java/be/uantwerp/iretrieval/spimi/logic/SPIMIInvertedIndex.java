package be.uantwerp.iretrieval.spimi.logic;

import be.uantwerp.iretrieval.spimi.dataTransfer.Constants;
import be.uantwerp.iretrieval.spimi.dataTransfer.ConstantsMemorySize;
import be.uantwerp.iretrieval.spimi.dataTransfer.Document;
import be.uantwerp.iretrieval.spimi.evaluation.EvaluationTimesSpimi;
import be.uantwerp.iretrieval.spimi.utility.FileUtility;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import be.uantwerp.iretrieval.spimi.utility.CustomArrayList;
import be.uantwerp.iretrieval.spimi.utility.MemorySizeStimation;
import java.util.Iterator;
import java.util.Map;

public class SPIMIInvertedIndex {

    private int TotalBlockCounter = 0;
    private int flushCounter = 1;
    private int currentSize = 0;
    private final MemorySizeStimation mse = new MemorySizeStimation();
    private JSONArray controller = new JSONArray();
    private final Constants constant = new Constants();
    private long timeMeasureSPIMI = 0;
    private long timeMeasureIn = 0;
    private long timeMeasureOut = 0;
    private long timeMeasureMemoryStimation = 0;
    private int totalBlocks = 0;

    public SPIMIInvertedIndex() {
    }

    private JSONObject createJsonController(int level, int block, int subblock) {
        JSONObject json = new JSONObject();
        json.put("level", level);
        json.put("block", block);
        json.put("subblock", subblock);
        return json;
    }

    private TreeMap<String, CustomArrayList<Long>> flushBlocksToDisk(TreeMap<String, CustomArrayList<Long>> dictionary) throws IOException {
        divideDictionaryToBlocks(dictionary);
        TreeMap<String, CustomArrayList<Long>> newDictionary = new TreeMap<>();
        controller.add(createJsonController(0, flushCounter, TotalBlockCounter));
        allocateVariables();
        return newDictionary;

    }

    private void allocateVariables() {
        currentSize = 0;
        flushCounter++;
        TotalBlockCounter = 0;
    }

    public TreeMap<String, CustomArrayList<Long>> addToDictionary(String token, Long docId, TreeMap<String, CustomArrayList<Long>> dictionary) throws IOException {
        if (currentSize > ConstantsMemorySize.getMAXIMUMSIZEMEMORY()) {
            dictionary = flushBlocksToDisk(dictionary);
        }
        if (!dictionary.isEmpty() && dictionary.containsKey(token)) {
            if (!dictionary.get(token).contains(docId)) {
                dictionary.get(token).add(docId);
            }
        } else {
            CustomArrayList<Long> postingList = new CustomArrayList<>();
            postingList.add(docId);
            dictionary.put(token, postingList);
        }
        return dictionary;
    }

    public EvaluationTimesSpimi InitalizeIndexConstruction() {
        //Initial time for the index construction
        long initialTime = System.currentTimeMillis();
        EvaluationTimesSpimi evalSpimi = new EvaluationTimesSpimi();
        JSONParser parser = new JSONParser();
        TreeMap<String, CustomArrayList<Long>> dictionary = new TreeMap<>();
        try {
            long initialInTime = System.currentTimeMillis();
            Object obj = parser.parse(new FileReader(constant.GET_NAMELISTDOCUMENTS()));
            timeMeasureIn = timeMeasureIn+(System.currentTimeMillis()-initialInTime);
            JSONArray jsonDocList = (JSONArray) obj;
            for (int i = 0; i < jsonDocList.size(); i++) {
                JSONObject jsonObj = (JSONObject) jsonDocList.get(i);
                Document document = new Document((Long) jsonObj.get("documentId"), (String) jsonObj.get("documentName"), (String) jsonObj.get("documentURL"));
                dictionary = createIndex(document, dictionary);
                long initialMemStimation = System.currentTimeMillis();
                currentSize = mse.sizeOfDictionary(dictionary);
                timeMeasureMemoryStimation = timeMeasureMemoryStimation+(System.currentTimeMillis()-initialMemStimation);
            }
            if (!dictionary.isEmpty()) {
                flushBlocksToDisk(dictionary);
            }
            System.out.println("Index creation finalized. Total flushes " + --flushCounter);
        } catch (FileNotFoundException e) {
        } catch (IOException | ParseException e) {
        }
        timeMeasureSPIMI = System.currentTimeMillis()-initialTime;
        evalSpimi.setResult(controller);
        evalSpimi.setTotalTime(timeMeasureSPIMI);
        evalSpimi.setMemoryStimationTime(timeMeasureMemoryStimation);
        evalSpimi.setInTime(timeMeasureIn);
        evalSpimi.setOutTime(timeMeasureOut);
        evalSpimi.setNumberOfResultingBlocks(totalBlocks);
        return evalSpimi;
    }

    public TreeMap<String, CustomArrayList<Long>> createIndex(Document doc, TreeMap<String, CustomArrayList<Long>> dictionary) {
        try {
            File inputFile = new File(doc.documentURL);
            FileReader fstream = new FileReader(inputFile);
            BufferedReader in = new BufferedReader(fstream);
            String line = in.readLine();
            while (line != null) {
                StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreTokens()) {
                    dictionary = addToDictionary(st.nextToken(), doc.documentId, dictionary);
                }
                line = in.readLine();
            }
            in.close();
        } catch (IOException e) {
        }
        return dictionary;
    }

    private void divideDictionaryToBlocks(TreeMap<String, CustomArrayList<Long>> dictionary) throws IOException {
        Set set = dictionary.entrySet();
        Iterator it = set.iterator();
        String arrayToDisk = "";
        boolean isFirst = true;
        while (it.hasNext()) {
            Map.Entry me = (Map.Entry) it.next();
            if (isFirst) {
                arrayToDisk = arrayToDisk + "{\"" + (String) me.getKey() + "\":" + ((CustomArrayList) me.getValue()).listToString() + "}";
                isFirst = false;
            } else {
                arrayToDisk = arrayToDisk + ",{\"" + (String) me.getKey() + "\":" + ((CustomArrayList) me.getValue()).listToString() + "}";
            }
            if (arrayToDisk.length() >= ConstantsMemorySize.getSIZEOFBLOCK()) {
                FlushBlockString("[" + arrayToDisk + "]");
                arrayToDisk = "";
                isFirst = true;
            }
        }
        if (!arrayToDisk.equals("")) {
            FlushBlockString("[" + arrayToDisk + "]");
        }
    }

    public void FlushBlockString(String dictionary) throws IOException {
        TotalBlockCounter++;
        totalBlocks++;
        long initialOutTime = System.currentTimeMillis();
        FileUtility.writeDocument(constant.GET_INVERTEDINDEXJSON() + "_" + flushCounter + "_" + TotalBlockCounter + ".spimi", dictionary);
        timeMeasureOut = timeMeasureOut+(System.currentTimeMillis()-initialOutTime);
        dictionary = "";

    }
}
