package be.uantwerp.iretrieval.spimi.logic;

import java.io.FileReader;
import java.util.ArrayList;
import me.lemire.integercompression.*;
import me.lemire.integercompression.differential.*;
import java.util.*;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import be.uantwerp.iretrieval.spimi.dataTransfer.Constants;
import be.uantwerp.iretrieval.spimi.dataTransfer.ConstantsMemorySize;
import be.uantwerp.iretrieval.spimi.evaluation.EvaluationTimesCompression;
import be.uantwerp.iretrieval.spimi.utility.FileUtility;
import be.uantwerp.iretrieval.spimi.utility.MemorySizeStimation;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author AddisBibish
 */
public class PostingListCompressor {

    private Constants constant = new Constants();
    private JSONParser parser = new JSONParser();
    private MemorySizeStimation memoryEstm = new MemorySizeStimation();
    private int count = 0;
    private int entriesSize = 0;
    JSONArray resultSet = new JSONArray();
    private long timeMeasureCompression = 0;
    private long timeMeasureIn = 0;
    private long timeMeasureOut = 0;
    private long timeMeasureMemoryStimation = 0;
    private int numberOfResultingBlocks = 0;

    public PostingListCompressor() {
    }

    public EvaluationTimesCompression compressor(int numberOfIndexes) throws IOException, ParseException {
        EvaluationTimesCompression evalCompression = new EvaluationTimesCompression();
        long initialTime = System.currentTimeMillis();
        Object obj = parser.parse(new FileReader(constant.GET_NAMELISTDOCUMENTS()));
        constant.SET_DELTAMAX(100);
//        System.out.println("Delta max "+constant.GET_DELTAMAX());
        IntegratedVariableByte vbe = new IntegratedVariableByte();
        resultSet = new JSONArray();
        for (int i = 1; i <= numberOfIndexes; i++) {
            long iniIntime = System.currentTimeMillis();
            File block = new File(constant.GET_SPIMIINDEXFINAL() + "_" + i + ".spimi");
            timeMeasureIn = timeMeasureIn + (System.currentTimeMillis() - iniIntime);
            Object object = parser.parse(new FileReader(block));
            JSONArray jsonBlock = (JSONArray) object;
            JSONArray data = null;
            for (int j = 0; j < jsonBlock.size(); j++) {
                JSONObject entry = (JSONObject) jsonBlock.get(j);
                Set keySet = entry.keySet();
                String entryKey = null;
                for (Object key : keySet) {
                    entryKey = (String) key;
                    data = (JSONArray) entry.get(key);
                }
                int[] postingList = JsonArrayToInt(data);
                byte[] compressed = new byte[postingList.length + constant.GET_DELTAMAX()];
                IntWrapper inputoffset = new IntWrapper(0);
                IntWrapper outputoffset = new IntWrapper(0);
                vbe.compress(postingList, inputoffset, postingList.length, compressed, outputoffset);
                compressed = Arrays.copyOf(compressed, outputoffset.intValue());
                JSONObject compressedEntry = new JSONObject();
                compressedEntry.put(entryKey, compressed);
                long iniMemStimation = System.currentTimeMillis();
                entriesSize = entriesSize
                        + memoryEstm.getSizeOfCompressedEntry(compressed.length, entryKey)
                        + memoryEstm.getSizeOfArrayAvg();
                timeMeasureMemoryStimation = timeMeasureMemoryStimation + (System.currentTimeMillis() - iniMemStimation);
                if (entriesSize > ConstantsMemorySize.getMAXIMUMSIZEMEMORY()) {
//                        System.out.println("********************************** NEW BLOCK *****************************************************");
                    jsonArrayToString(resultSet);
                    resultSet = new JSONArray();
                    entriesSize = 0;
                } else {
                    resultSet.add(compressedEntry);
                }
            }
        }
        if (!resultSet.isEmpty()) {
            jsonArrayToString(resultSet);
        }
        timeMeasureCompression = System.currentTimeMillis() - initialTime;
        evalCompression.setTotalTime(timeMeasureCompression);
        evalCompression.setInTime(timeMeasureIn);
        evalCompression.setOutTime(timeMeasureOut);
        evalCompression.setMemoryStimationTime(timeMeasureMemoryStimation);
        evalCompression.setNumberOfResultingBlocks(numberOfResultingBlocks);
        return evalCompression;
    }

    public JSONArray Decompressor(JSONArray block) {
        JSONArray result = new JSONArray();
        int[] recovered = null;
        IntegratedVariableByte vbe = new IntegratedVariableByte();
        for (int i = 0; i < block.size(); i++) {
            JSONObject entry = (JSONObject) block.get(i);
            Set keySet = entry.keySet();
            String entryKey = null;
            JSONArray postingList = null;
            for (Object key : keySet) {
                entryKey = (String) key;
                postingList = (JSONArray) entry.get(key);
            }
            byte[] compressed = JsonArrayToByte(postingList);
            recovered = new int[postingList.size()];
            IntWrapper recoffset = new IntWrapper(0);
            vbe.uncompress(compressed, new IntWrapper(0), compressed.length, recovered, recoffset);
            JSONObject uncompressedEntry = new JSONObject();
            JSONArray uncompressed = IntToJsonArray(recovered);
            int lastIndex = uncompressed.size() - 1;
            for (int k = lastIndex; k >= (lastIndex / 2); k--) {
                if ((long) uncompressed.get(k) == 0) {
                    uncompressed.remove(k);
                }
            }
            uncompressedEntry.put(entryKey, uncompressed);
            result.add(uncompressedEntry);
        }

        return result;
    }

    public int[] JsonArrayToInt(JSONArray array) {
        int[] data = new int[array.size()];
        for (int i = 0; i < array.size(); i++) {
            data[i] = (int) (long) array.get(i);
        }
        return data;
    }

    public byte[] JsonArrayToByte(JSONArray array) {
        byte[] data = new byte[array.size()];
        for (int i = 0; i < array.size(); i++) {
            data[i] = (byte) (long) array.get(i);
        }
        return data;
    }

    public JSONArray IntToJsonArray(int[] array) {
        JSONArray data = new JSONArray();
        for (int i = 0; i < array.length; i++) {
            data.add((long) array[i]);
        }
        return data;
    }

    public int[] ByteToIntArray(byte[] byteArray) {
        IntBuffer intBuf
                = ByteBuffer.wrap(byteArray)
                .order(ByteOrder.BIG_ENDIAN)
                .asIntBuffer();
        int[] array = new int[intBuf.remaining()];
        intBuf.get(array);
        return array;
    }

    public void jsonArrayToString(JSONArray jsonBlock) throws IOException {
        String jsonFormat = "";
        for (int i = 0; i < jsonBlock.size(); i++) {
            JSONObject entry = (JSONObject) jsonBlock.get(i);
            Set keySet = entry.keySet();
            String entryKey = null;
            byte[] data = null;
            for (Object key : keySet) {
                entryKey = (String) key;
                data = (byte[]) entry.get(key);
            }
            String postingList = "{\"" + entryKey + "\":[";
            for (int j = 0; j < data.length; j++) {
                if (j == 0) {
                    postingList = postingList + data[j];
                } else {
                    postingList = postingList + "," + data[j];
                }
            }
            postingList = postingList + "]}";
            if (jsonFormat.equals("")) {
                jsonFormat = "[" + postingList;
            } else {
                jsonFormat = jsonFormat + "," + postingList;
            }

            if (jsonFormat.length() >= ConstantsMemorySize.getSIZEOFBLOCK()) {
                jsonFormat = jsonFormat + "]";
                FlushBlockString(jsonFormat);
                jsonFormat = "";
            }

        }
        if (jsonFormat.length() != 0) {
            jsonFormat = jsonFormat + "]";
            FlushBlockString(jsonFormat);
        }
    }

    public void FlushBlockString(String dictionary) throws IOException {
        long iniOutTime = System.currentTimeMillis();
        FileUtility.writeDocument(constant.GET_FINALINDEXCOMPRESSED() + "_" + (++count) + ".spimi", dictionary);
        timeMeasureOut = timeMeasureOut + (System.currentTimeMillis() - iniOutTime);
        numberOfResultingBlocks++;
    }

}
