package be.uantwerp.iretrieval.spimi.logic;

import be.uantwerp.iretrieval.spimi.dataTransfer.Constants;
import be.uantwerp.iretrieval.spimi.dataTransfer.ConstantsMemorySize;
import be.uantwerp.iretrieval.spimi.evaluation.EvaluationTimesMerging;
import be.uantwerp.iretrieval.spimi.utility.MemorySizeStimation;
import be.uantwerp.iretrieval.spimi.utility.BlockStimation;
import be.uantwerp.iretrieval.spimi.utility.FileUtility;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.apache.log4j.Logger;

public class MergeSPIMIInvertedIndex {

    private int SuperSubBlockCounter = 1;
    private int SuperBlockCounter = 1;
    private int numberOfBlocks = 0;
    private int totalOfLevels;
    private int currentLevel = 1;
    private JSONArray controller = new JSONArray();
    private JSONArray result = new JSONArray();
    private JSONArray residue = new JSONArray();
    private final MemorySizeStimation mse = new MemorySizeStimation();
    private int numberOfIdexesFinal = 0;

    private int sizeOfDictionary = 0;

    private final Constants constant = new Constants();

    private final Logger log = Logger.getLogger(MergeSPIMIInvertedIndex.class.getName());
    
    private long timeMeasureMerge = 0;
    private long timeMeasureIn = 0;
    private long timeMeasureOut = 0;
    private long timeMeasureMemoryStimation = 0;

    public MergeSPIMIInvertedIndex() {
    }

    public EvaluationTimesMerging InitalizeMerge(JSONArray controller) throws FileNotFoundException, IOException, ParseException {
        EvaluationTimesMerging evalMerging = new EvaluationTimesMerging();
        long initialTime = System.currentTimeMillis();
        org.apache.log4j.BasicConfigurator.configure();
        currentLevel = 1;
        this.controller = controller;
        numberOfBlocks = BlockStimation.numberOfLevels(this.controller);
        totalOfLevels = Math.round(numberOfBlocks / 2);
        int numberOfLevel = totalOfLevels;
        while (numberOfLevel >= 1) {
            int currentBlock = 1;
            int numberOfBlocksLevel = BlockStimation.numberOfBlocks(this.controller, currentLevel - 1);
            String path;
            if (currentLevel == 1) {
                path = constant.GET_SPIMIINDEXBLOCKS() + "\\spimiIndex_";
            } else {
                path = constant.GET_SUPERBLOCKS() + "_" + (currentLevel - 1) + "_";
            }
            while (currentBlock <= numberOfBlocksLevel) {
                int numberOfSubBlock1 = BlockStimation.numberOfSubBlocks(this.controller, currentLevel - 1, currentBlock);
                int fixedNumberBlock1 = numberOfSubBlock1;
                int numberOfSubBlock2 = -1;
                int fixedNumberBlock2 = -1;
                if (currentBlock + 1 <= numberOfBlocksLevel) {
                    numberOfSubBlock2 = BlockStimation.numberOfSubBlocks(this.controller, currentLevel - 1, currentBlock + 1);
                    fixedNumberBlock2 = numberOfSubBlock2;
                }
                int currentSubBlock1 = 1;
                int currentSubBlock2 = 1;
                int residueSubBlock = -1;
                while (currentSubBlock1 != (fixedNumberBlock1 + 1) || (currentSubBlock2 != (fixedNumberBlock2 + 1))) {
                    if (currentSubBlock1 == 1 && currentSubBlock2 == 1 && numberOfSubBlock2 != -1) {
                        log.debug("charges block: " + (path + (currentBlock) + "_" + currentSubBlock1 + ".spimi"));
                        log.debug("charges block: " + (path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi"));
                        long inTime = System.currentTimeMillis();
                        JSONArray firstSubBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock) + "_" + currentSubBlock1 + ".spimi");
                        JSONArray secondSubBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi");
                        timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                        residueSubBlock = MergeBlocks(firstSubBlockJson, secondSubBlockJson, currentBlock, currentBlock + 1);
                        currentSubBlock2++;
                        numberOfSubBlock2--;
                        currentSubBlock1++;
                        numberOfSubBlock1--;
                    } else if (numberOfSubBlock2 == -1) {
                        log.debug("charges block: " + (path + (currentBlock) + "_" + currentSubBlock1 + ".spimi"));
                        long inTime = System.currentTimeMillis();
                        JSONArray subBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock) + "_" + currentSubBlock1 + ".spimi");
                        timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                        FlushBlock(subBlockJson);
                        currentSubBlock1++;
                        numberOfSubBlock1--;
                        currentSubBlock2 = fixedNumberBlock2 + 1;
                    } else if (residueSubBlock != -1) {
                        if (residueSubBlock == currentBlock) {//the residue is in the first block
                            if (numberOfSubBlock2 > 0) {
                                log.debug("charges block: " + (path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi"));
                                long inTime = System.currentTimeMillis();
                                timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                                JSONArray subBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi");
                                residueSubBlock = MergeBlocks(residue, subBlockJson, currentBlock, currentBlock + 1);
                                numberOfSubBlock2--;
                                currentSubBlock2++;
                            } else {
                                log.debug("charges block: " + (path + (currentBlock) + "_" + currentSubBlock1 + ".spimi"));
                                long inTime = System.currentTimeMillis();
                                timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                                JSONArray subBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock) + "_" + currentSubBlock1 + ".spimi");
                                residueSubBlock = MergeBlocks(subBlockJson, residue, currentBlock, currentBlock + 1);
                                numberOfSubBlock1--;
                                currentSubBlock1++;
                            }
                        } else if (residueSubBlock == currentBlock + 1) {//the residue is in the second block
                            if (numberOfSubBlock1 > 0) {
                                log.debug("charges block: " + (path + (currentBlock) + "_" + currentSubBlock1 + ".spimi"));
                                long inTime = System.currentTimeMillis();
                                JSONArray subBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock) + "_" + currentSubBlock1 + ".spimi");
                                timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                                residueSubBlock = MergeBlocks(subBlockJson, residue, currentBlock, currentBlock + 1);
                                numberOfSubBlock1--;
                                currentSubBlock1++;
                            } else {
                                log.debug("charges block: " + (path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi"));
                                long inTime = System.currentTimeMillis();
                                JSONArray subBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi");
                                timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                                residueSubBlock = MergeBlocks(residue, subBlockJson, currentBlock, currentBlock + 1);
                                numberOfSubBlock2--;
                                currentSubBlock2++;
                            }
                        }
                    } else if (numberOfSubBlock2 > 0 && numberOfSubBlock1 > 0) {
                        log.debug("charges block: " + (path + (currentBlock) + "_" + currentSubBlock1 + ".spimi"));
                        log.debug("charges block: " + (path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi"));
                        long inTime = System.currentTimeMillis();
                        JSONArray firstSubBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock) + "_" + currentSubBlock1 + ".spimi");
                        JSONArray secondSubBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi");
                        timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                        residueSubBlock = MergeBlocks(firstSubBlockJson, secondSubBlockJson, currentBlock, currentBlock + 1);
                        currentSubBlock2++;
                        numberOfSubBlock2--;
                        currentSubBlock1++;
                        numberOfSubBlock1--;
                    } else if ((numberOfSubBlock1 > 0) && !(numberOfSubBlock2 > 0)) {
                        log.debug("charges block: " + (path + (currentBlock) + "_" + currentSubBlock1 + ".spimi"));
                        long inTime = System.currentTimeMillis();
                        JSONArray subBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock) + "_" + currentSubBlock1 + ".spimi");
                        timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                        residueSubBlock = MergeBlocks(subBlockJson);
                        numberOfSubBlock1--;
                        currentSubBlock1++;
                    } else if (!(numberOfSubBlock1 > 0) && (numberOfSubBlock2 > 0)) {
                        log.debug("charges block: " + (path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi"));
                        long inTime = System.currentTimeMillis();
                        JSONArray subBlockJson = FileUtility.getJsonArrayFromFile(path + (currentBlock + 1) + "_" + currentSubBlock2 + ".spimi");
                        timeMeasureIn = timeMeasureIn +(System.currentTimeMillis()-inTime);
                        residueSubBlock = MergeBlocks(subBlockJson);
                        numberOfSubBlock2--;
                        currentSubBlock2++;
                    }
                }
                MergeBlocks(result, residue);
                if (currentLevel == totalOfLevels) {
                    numberOfIdexesFinal = SuperSubBlockCounter;
                }
                SuperSubBlockCounter = 1;
                SuperBlockCounter++;
                if (currentBlock + 1 <= numberOfBlocksLevel) {
                    currentBlock = currentBlock + 2;
                } else {
                    currentBlock++;
                }
            }

            SuperBlockCounter = 1;
            currentLevel++;
            numberOfLevel--;
        }
        timeMeasureMerge = System.currentTimeMillis()-initialTime;
        evalMerging.setNumberEntries(numberOfIdexesFinal-1);
        evalMerging.setTotalTime(timeMeasureMerge);
        evalMerging.setInTime(timeMeasureIn);
        evalMerging.setOutTime(timeMeasureOut);
        evalMerging.setMemoryStimationTime(0);
        return evalMerging;
    }

    private int MergeBlocks(JSONArray b1, JSONArray b2, int numBlock1, int numBlock2) throws ParseException, IOException {
        residue = new JSONArray();
        int i = 0;
        int j = 0;
        while (i < b1.size() && j < b2.size()) {

            JSONObject blockEntry1 = (JSONObject) b1.get(i);
            String firstBlockKey = getKeyValue(blockEntry1);
            JSONArray entry1PList = (JSONArray) blockEntry1.get(firstBlockKey);

            JSONObject blockEntry2 = (JSONObject) b2.get(j);
            String secondBlockKey = getKeyValue(blockEntry2);
            JSONArray entry2PList = (JSONArray) blockEntry2.get(secondBlockKey);
            if (sizeOfDictionary >= ConstantsMemorySize.getSIZEOFBLOCK()) {
                sizeOfDictionary = 0;
                FlushBlock(result);
                result = new JSONArray();
            }
            if (firstBlockKey.compareTo(secondBlockKey) < 0) { //term in block 1 is minor than term in block 2
                result.add(blockEntry1);
                sizeOfDictionary = sizeOfDictionary + blockEntry1.toString().length();
                i++;
            } else if (firstBlockKey.compareTo(secondBlockKey) > 0) {//term in block 2 is minor than term in block 1
                result.add(blockEntry2);
                sizeOfDictionary = sizeOfDictionary + entry2PList.toString().length();
                j++;
            } else {//terms are equal    
                entry1PList.addAll(entry2PList);
                JSONObject appendedJson = new JSONObject();
                appendedJson.put(firstBlockKey, entry1PList);
                result.add(appendedJson);
                sizeOfDictionary = sizeOfDictionary + appendedJson.toString().length();
                i++;
                j++;
            }
        }
        if (i < b1.size()) {
            while (i < b1.size()) {
                residue.add((JSONObject) b1.get(i));
                i++;
            }
            return numBlock1;
        } else if (j < b2.size()) {
            while (j < b2.size()) {
                residue.add((JSONObject) b2.get(j));
                j++;
            }
            return numBlock2;
        }
        return -1;
    }

    private void MergeBlocks(JSONArray b1, JSONArray b2) throws ParseException, IOException {
        System.out.println("enter to merge final");
        JSONArray resultFinal = new JSONArray();
        int sizeOfDictionaryF = 0;
        int i = 0;
        int j = 0;
        while (i < b1.size() && j < b2.size()) {

            JSONObject blockEntry1 = (JSONObject) b1.get(i);
            String firstBlockKey = getKeyValue(blockEntry1);
            JSONArray entry1PList = (JSONArray) blockEntry1.get(firstBlockKey);
            if (sizeOfDictionaryF > ConstantsMemorySize.getSIZEOFBLOCK()) {
                FlushBlock(resultFinal);
                resultFinal = new JSONArray();
                sizeOfDictionaryF = 0;
            }

            JSONObject blockEntry2 = (JSONObject) b2.get(j);
            String secondBlockKey = getKeyValue(blockEntry2);
            JSONArray entry2PList = (JSONArray) blockEntry2.get(secondBlockKey);

            if (firstBlockKey.compareTo(secondBlockKey) < 0) { //term in block 1 is minor than term in block 2
                sizeOfDictionaryF = sizeOfDictionaryF + blockEntry1.toString().length();
                resultFinal.add(blockEntry1);
                i++;
            } else if (firstBlockKey.compareTo(secondBlockKey) > 0) {//term in block 2 is minor than term in block 1
                sizeOfDictionaryF = sizeOfDictionaryF + blockEntry2.toString().length();
                resultFinal.add(blockEntry2);
                j++;
            } else {//terms are equal                    
                entry1PList.addAll(entry2PList);
                JSONObject appendedJson = new JSONObject();
                appendedJson.put(firstBlockKey, entry1PList);
                resultFinal.add(appendedJson);
                sizeOfDictionaryF = sizeOfDictionaryF + appendedJson.toString().length();
                i++;
                j++;
            }
        }
        if (i < b1.size()) {
            while (i < b1.size()) {
                JSONObject blockEntry1 = (JSONObject) b1.get(i);
                String firstBlockKey = getKeyValue(blockEntry1);
                JSONArray entry1PList = (JSONArray) blockEntry1.get(firstBlockKey);
                sizeOfDictionaryF = sizeOfDictionaryF + mse.getSizeOfLArray(entry1PList.size(), firstBlockKey);
                if (sizeOfDictionaryF > ConstantsMemorySize.getSIZEOFBLOCK()) {
                    FlushBlock(resultFinal);
                    resultFinal = new JSONArray();
                    sizeOfDictionaryF = 0;
                }
                resultFinal.add(blockEntry1);
                i++;
            }
        } else if (j < b2.size()) {
            while (j < b2.size()) {
                JSONObject blockEntry2 = (JSONObject) b2.get(j);
                String secondBlockKey = getKeyValue(blockEntry2);
                JSONArray entry2PList = (JSONArray) blockEntry2.get(secondBlockKey);
                sizeOfDictionaryF = sizeOfDictionaryF + mse.getSizeOfLArray(entry2PList.size(), secondBlockKey);
                if (sizeOfDictionaryF > ConstantsMemorySize.getSIZEOFBLOCK()) {
                    FlushBlock(resultFinal);
                    resultFinal = new JSONArray();
                    sizeOfDictionaryF = 0;
                }
                resultFinal.add(blockEntry2);
                j++;
            }
        }
        if (!resultFinal.isEmpty()) {
            FlushBlock(resultFinal);
        }
        result = new JSONArray();
        residue = new JSONArray();
        sizeOfDictionary = 0;
    }

    private int MergeBlocks(JSONArray b1) throws ParseException, IOException {
        System.out.println("enters to merge 2*********************");
        int i = 0;
        int j = 0;
        while (i < b1.size()) {
            JSONObject blockEntry1 = (JSONObject) b1.get(i);
            if (sizeOfDictionary > ConstantsMemorySize.getSIZEOFBLOCK()) {
                FlushBlock(result);
                result = new JSONArray();
            }
            result.add(blockEntry1);
            sizeOfDictionary = sizeOfDictionary + blockEntry1.toString().length();
            i++;
        }
        residue = new JSONArray();
        return -1;
    }

    private String getKeyValue(JSONObject entry) {
        Set<String> block1Key = entry.keySet();
        String secondBlockKey = "";
        for (String key : block1Key) {
            secondBlockKey = key;
            break;
        }
        return secondBlockKey;
    }

    private void FlushBlock(JSONArray dictionary) throws IOException {
        
        if (currentLevel == totalOfLevels) {
            long iniOutTime = System.currentTimeMillis();
            FileUtility.writeDocument(constant.GET_SPIMIINDEXFINAL() + "_" + SuperSubBlockCounter + ".spimi", dictionary.toJSONString());
            timeMeasureOut = timeMeasureOut +(System.currentTimeMillis()-iniOutTime);
        } else {
            long iniOutTime = System.currentTimeMillis();
            FileUtility.writeDocument(constant.GET_SUPERBLOCKS() + "_" + (currentLevel) + "_" + SuperBlockCounter + "_" + SuperSubBlockCounter + ".spimi", dictionary.toJSONString());
            timeMeasureOut = timeMeasureOut +(System.currentTimeMillis()-iniOutTime);
        }
        
        dictionary.clear();
        addToController();
        SuperSubBlockCounter++;
    }

    private void addToController() {
        JSONObject newJsonController = new JSONObject();
        newJsonController.put("level", currentLevel);
        newJsonController.put("block", SuperBlockCounter);
        newJsonController.put("subblock", SuperSubBlockCounter);
        controller.add(newJsonController);
    }

}
