/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.utility;

import be.uantwerp.iretrieval.spimi.dataTransfer.ConstantsMemorySize;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author gerito
 */
public class MemorySizeStimation {

    private int sizeOfArryAverage = 0;

    public MemorySizeStimation() {
        setSizeOfArrayAverage();
    }

    public int getSizeOfArrayAvg() {
        return this.sizeOfArryAverage;
    }

    private void setSizeOfArrayAverage() {
        sizeOfArryAverage = ConstantsMemorySize.getSIZEOFINT()
                + //Size of reference of the array itself
                ConstantsMemorySize.getSIZEOFHEADERS()
                + //Size of the header of the array
                (ConstantsMemorySize.getSIZEOFINT() * 2)
                + //two primitive types inside the Array List totalCapacity
                (ConstantsMemorySize.getSIZEOFHEADERS() * 2)
                + //headers of the integers in the list
                (ConstantsMemorySize.getSIZEOFALIGNMENT() * 2);//alignment of the integers in the list
    }

    public int getSizeOfLArray(int numerOfElements, String key) {
        return sizeOfArryAverage
                +/**
                 * ***************cost for the array****************
                 */
                /**
                 * ***************cost for the size of string******
                 */
                key.length() * ConstantsMemorySize.getSIZEOFCHAR()
                + //size of the key stored in the dictionary(constantMS.getSIZEOFLONG()*numerofElements) + //Size of the long primitive types stores in the list
                /**
                 * *********************************************
                 */
                (ConstantsMemorySize.getSIZEOFINT() * numerOfElements)
                + //References made of the total capacity
                (ConstantsMemorySize.getSIZEOFINT() * getNextBase2Naive(numerOfElements))
                + //References made of the total capacity
                (ConstantsMemorySize.getSIZEOFHEADERS() * numerOfElements)
                + //headers of the object
                (ConstantsMemorySize.getSIZEOFALIGNMENT() * numerOfElements); //bytes lost per allignment
    }

    private int getNextBase2Naive(int number) {
        int base2Number = 1;
        while (base2Number < number) {
            if (base2Number == 1) {
                base2Number = 2;
            } else {
                base2Number = base2Number * 2;
            }
        }
        return base2Number;
    }

    public int getSizeOfCompressedEntry(int numerOfElements, String key) {
        return ConstantsMemorySize.getSIZEOFBYTE() * numerOfElements
                + //Size of reference of the dictionary itself
                ConstantsMemorySize.getSIZEOFHEADERS() * numerOfElements
                + //Size of the header of the dictionary
                ConstantsMemorySize.getSIZEOFALIGNMENT() * numerOfElements
                + /**
                 * ***************cost for the size of string******
                 */
                key.length() * ConstantsMemorySize.getSIZEOFCHAR(); //size of the key stored in the dictionary(constantMS.getSIZEOFLONG()*numerofElements) + //Size of the long primitive types stores in the list
    }

    public int getSizeOfCustomArrayList(int numberOfElements, int totalCapacity, int sizeOfString) {
        int size
                = /**
                 * ***************cost for the array****************
                 */
                sizeOfArryAverage
                + //alignment of the integers in the list
                /**
                 * ***************cost for the size of string******
                 */
                sizeOfString
                + //size of the key stored in the dictionary
                /**
                 * ***************cost for the primitive types******
                 */
                (ConstantsMemorySize.getSIZEOFLONG() * numberOfElements)
                + //Size of the long primitive types stores in the list
                (ConstantsMemorySize.getSIZEOFINT() * totalCapacity)
                + //References made of the total capacity
                (ConstantsMemorySize.getSIZEOFHEADERS() * numberOfElements)
                + //headers of the object
                (ConstantsMemorySize.getSIZEOFALIGNMENT() * numberOfElements) //bytes lost per allignment
                ;
        return size;
    }

    public int getSizeOfLArrayString(int numerOfElements, String key) {
        return /**
                 * ***************cost for the size of string******
                 */
                8
                +//"two {"":[]}"
                key.length()
                + //size of the key stored in the dictionary(constantMS.getSIZEOFLONG()*numerofElements) + //Size of the long primitive types stores in the list
                /**
                 * *********************************************
                 */
                numerOfElements
                + //number of elementes
                numerOfElements - 1;//number of comas
    }

    public int sizeOfDictionary(TreeMap<String, CustomArrayList<Long>> dictionary) throws java.io.IOException {
        Set set = dictionary.entrySet();
        Iterator it = set.iterator();
        int totalSize = 0;
        /**
         * ***************size of dictionary itself****************
         */
        totalSize = ConstantsMemorySize.getSIZEOFINT()
                + //Size of reference of the dictionary itself
                ConstantsMemorySize.getSIZEOFHEADERS()
                + //Size of the header of the dictionary
                ConstantsMemorySize.getSIZEOFALIGNMENT(); //alignment of the dictionary
        while (it.hasNext()) {
            Map.Entry me = (Map.Entry) it.next();
            CustomArrayList list = (CustomArrayList) me.getValue();
            String key = (String) me.getKey();
            int sizeOfString = key.length() * ConstantsMemorySize.getSIZEOFCHAR();
            totalSize = totalSize + getSizeOfCustomArrayList(list.size(), list.totalCapacity(), sizeOfString);
        }
        return totalSize;
    }
    
    public int getSizeOfUploadEntry(int numerOfElements,String key){
        return  sizeOfArryAverage+/*****************cost for the array*****************/
                /*****************cost for the size of string*******/
                key.length()*ConstantsMemorySize.getSIZEOFCHAR() + //size of the key stored in the dictionary(constantMS.getSIZEOFLONG()*numerofElements) + //Size of the long primitive types stores in the list
                /************************************************/
                (ConstantsMemorySize.getSIZEOFINT()*numerOfElements) + //References made of the total capacit
                (ConstantsMemorySize.getSIZEOFHEADERS()*numerOfElements) + //headers of the object
                (ConstantsMemorySize.getSIZEOFALIGNMENT()*numerOfElements); //bytes lost per allignment
    }
    
}
