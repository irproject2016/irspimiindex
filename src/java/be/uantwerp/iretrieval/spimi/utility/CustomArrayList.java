package be.uantwerp.iretrieval.spimi.utility;

import java.util.Arrays;
import java.util.Collection;
import org.json.simple.JSONArray;

public class CustomArrayList<E>{
    public static final int DEFAULT_INITIAL_CAPACITY = 1;
    public static final Object[] EMPTY_ELEMENT_DATA = {};
    public int size;
    public int totalCapacity;

    /**
     * The array elements to be stored inside 
     * customArrayListElementData.
     */
    public transient Object[] customArrayListElementData;

    /**
     * Constructs a custom arrayList with an initial capacity.
     * @param initialCapacity
     */
    public CustomArrayList(int initialCapacity){
      super();
         if (initialCapacity < 0)
             throw new IllegalArgumentException("Illegal Capacity: "+
                                                initialCapacity);
         this.customArrayListElementData = new Object[initialCapacity];
    }
    
    /**
     * Constructs an empty list.
     */
    public CustomArrayList(){
       super();
          this.customArrayListElementData = EMPTY_ELEMENT_DATA;
    }
    
    /**
     * @return the size of the CustomArrayList
     */
    public int size() {
     return size;
    }
    
    public int totalCapacity() {
     return totalCapacity;
    }

    /**
     * @return true/false if size is greater then 0 return true else false.
     */
    public boolean isEmpty() {
        return size==0;
    }

    /**
     * return true 
     * @param e
        * @return 
     */
    public boolean add(E e) {
        ensureCapacity(size + 1);  
        customArrayListElementData[size++] = e;
        return true;
    }

    public void clear() {
        for (int i = 0; i < size; i++)
            customArrayListElementData[i] = null;
        totalCapacity = 0;
        size = 0;
  
    }
    /**
     * Returns the element at the specified position in this list.
     * @param index
     * @return
     */
    @SuppressWarnings("unchecked")
    public E get(int index) {
        if (index >= size){
            throw new ArrayIndexOutOfBoundsException("array index out of bound exception with index at"+index);
        }
        return (E)customArrayListElementData[index];
    }
 
    /**
     * add element at specific index position and shift the
     * customArrayListElementData.
     * @param index
     * @param element
     */
    public void add(int index, E element) {
        ensureCapacity(size + 1); 
        System.arraycopy(customArrayListElementData, index, customArrayListElementData, index + 1,size - index);
        customArrayListElementData[index] = element;
        size++;
  
    }

    /**
     * Remove the element from the customArrayListElementData
     * and shift the elements position.
     * @param index
     * @return
     */
    @SuppressWarnings("unchecked")
    public E remove(int index) {
    E oldValue = (E)customArrayListElementData[index];

        int removeNumber = size - index - 1;
        if (removeNumber > 0){
            System.arraycopy(customArrayListElementData, index+1, customArrayListElementData, index,removeNumber);
        }
        customArrayListElementData[--size] = null; 
        return oldValue;
 }

    /**
     * Increases the capacity to ensure that it can hold at least the
     * number of elements specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     */
    private void growCustomArrayList(int minCapacity) {
        int oldCapacity = customArrayListElementData.length;
        int newCapacity = oldCapacity *2;//+ (oldCapacity /2);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        totalCapacity = newCapacity;
        customArrayListElementData = Arrays.copyOf(customArrayListElementData, newCapacity);
    }
    
    /**
     * ensure the capacity and grow the customArrayList vi 
     * growCustomArrayList(minCapacity);
     * @param minCapacity
     */
    private void ensureCapacity(int minCapacity) {
        if (customArrayListElementData == EMPTY_ELEMENT_DATA) {
            minCapacity = Math.max(DEFAULT_INITIAL_CAPACITY, minCapacity);
        }

        if (minCapacity - customArrayListElementData.length > 0)
            growCustomArrayList(minCapacity);
    }
    

    public boolean contains(E e) {
        for (int i=0;i<size();i++)
            if (customArrayListElementData[i].equals(e))
                return true;
        return false;
    }
    
    public int indexOf(E e) {
        for (int i=0;i<size();i++)
            if (customArrayListElementData[i].equals(e))
                return i;
        return -1;
    }
    
    public boolean containsAll(Collection<E> collection) {
        return collection.stream().noneMatch((obj) -> (!contains(obj)));
    }
    
    public String listToString(){
        boolean isFist = true;
        String list = "[";
        for (int i=0; i<size(); i++){
            if (isFist){
               list = list+customArrayListElementData[i].toString(); 
               isFist = false;
            }else
                list = list+"," + customArrayListElementData[i].toString(); 
        }
        if (!list.equals(""))
            list = list+"]";
        if (size()==0)
            return null;
        else
            return list;
    }
    
    public JSONArray listToJSON(){
        JSONArray  json = new JSONArray();
        for (int i=0; i<size(); i++){
            json.add(customArrayListElementData[i]);
        }
        return json;
    }
    

}
 