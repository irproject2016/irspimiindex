/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.utility;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author gerito
 */
public abstract class BlockStimation {
    
    public static int numberOfLevels(JSONArray controller){
        JSONObject json = new JSONObject();
        for (int i=0; i<controller.size();i++)
            json = (JSONObject) controller.get(i);
//        numberOfBlocks = (Integer) json.get("block");
        return (Integer) json.get("block");
    }
    
    public static int numberOfBlocks(JSONArray controller, int level){
        int result = -1;
        for (int i=0; i<controller.size();i++){ 
            JSONObject json = (JSONObject) controller.get(i);
            if ((Integer) json.get("level") == level){
                result = (Integer) json.get("block");
            }
        }
        return result;
    }
    
    public static int numberOfSubBlocks(JSONArray controller, int level, int block){
        int result = -1;
        for (int i=0; i<controller.size();i++){    
            JSONObject json = (JSONObject) controller.get(i);
            if (((Integer) json.get("level")==level) && 
                ((Integer) json.get("block")==block)){
                result = (Integer) json.get("subblock");
            }
        }
        return result;
    }
    
}
