package be.uantwerp.iretrieval.spimi.utility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public abstract class JasonParser {
    
    public static JSONArray parseFile(File json) throws IOException, ParseException{
        JSONArray jsonArray = new JSONArray();
        JsonFactory f = new MappingJsonFactory();
        JsonParser jp = f.createJsonParser(json);
        JSONParser parser = new JSONParser();
        JsonToken current;

        current = jp.nextToken();
        if (current != JsonToken.START_OBJECT) {

          return null;
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {
//            System.out.println("first while");
            current = jp.nextToken();
            if (current == JsonToken.START_ARRAY) {
                while (jp.nextToken() != JsonToken.END_ARRAY) {
                    JsonNode node = jp.readValueAsTree();
                    JSONObject jsonP = (JSONObject) parser.parse(node.toString());
                    jsonArray.add(jsonP);
                }                
            }else {
                jp.skipChildren();
            }
        }
//        System.out.println(jsonArray.toJSONString());
        return jsonArray;
    }
}
