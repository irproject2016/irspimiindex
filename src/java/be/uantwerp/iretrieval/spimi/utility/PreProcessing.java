/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.utility;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.AttributeFactory;
import org.tartarus.snowball.ext.EnglishStemmer;

/**
 *
 * @author gerito
 */
public class PreProcessing {
    
    private HashMap<String,Integer> stopWords = new HashMap<>();

    public PreProcessing(String pathStopWords){
        try {
            getStopWords(pathStopWords);
        } catch (IOException ex) {
            Logger.getLogger(PreProcessing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public PreProcessing(){}
    
    public String documentTransformation(String document) throws IOException{
        StandardAnalyzer analyzer = new StandardAnalyzer();
        String documentTransformed = "";
        StandardTokenizer tokenizer = documentTokenization(document);
        CharTermAttribute attr = tokenizer.addAttribute(CharTermAttribute.class);
        while(tokenizer.incrementToken()) {
            // Grab the term
            String term = attr.toString().toLowerCase();
            /**Creating an own hashmap with the words actually improves the time a lot than doing 
             * it with lucene library, and lucene is not effective in a lot of cases**/
            if (!stopWords.containsKey(term))
                documentTransformed = documentTransformed + " " + wordPorterStemming(term);
        }
        return documentTransformed.trim();
    }
    public LinkedHashMap<String, ArrayList> QueryAnalyser(String query) throws IOException{
        String[] queryListInit = null;
        ArrayList keywordsList = new ArrayList<String>();
        ArrayList booleanOpList = new ArrayList<String>();
        ArrayList queryList = new ArrayList<String>();
        queryListInit = query.split(" ");
        for(String s : queryListInit){
            queryList.add(s);
        }
        int lastIndex = queryList.size()-1;
        if(queryList.get(0).equals("AND")||queryList.get(0).equals("OR")|| queryList.get(0).equals("NOT"))
            queryList.remove(0);
        else if(queryList.get(lastIndex).equals("AND")||queryList.get(lastIndex).equals("OR")|| queryList.get(lastIndex).equals("NOT"))
            queryList.remove(lastIndex);
        boolean eraseNext = false;
        for (Object keyword : queryList) {            
            if("AND".equals(keyword) || "OR".equals(keyword) ||"NOT".equals(keyword)){
                if (!eraseNext)
                    booleanOpList.add(keyword);
            }
            else {
                if (!stopWords.containsKey(((String) keyword).toLowerCase().trim())){                    
                    keywordsList.add(keyword); 
                }
                else{
                    if (!booleanOpList.isEmpty())
                            booleanOpList.remove(booleanOpList.size()-1);
                    else{
                        eraseNext = true;
                    }
                }
            }
        }
        String term = null;
        ArrayList transformedKeywordsList = new ArrayList();
        for(Object keyword : keywordsList){
            term = keyword.toString().toLowerCase();
            transformedKeywordsList.add(wordPorterStemming(term));   
        }
        LinkedHashMap <String, ArrayList> resultSet = new LinkedHashMap ();
        resultSet.put("booleanOp", booleanOpList);
        resultSet.put("queryList", transformedKeywordsList);
        
        return  resultSet;   
    }
   
    public StandardTokenizer getTokenizedDocument(String document) throws IOException{
        StandardTokenizer tokenizer = documentTokenization(document);
        return tokenizer;
    }
    
    public StandardTokenizer documentTokenization(String document) throws IOException{
        // Define your attribute factory (or use the default) - same between 4.x and 5.x
        AttributeFactory factory = AttributeFactory.DEFAULT_ATTRIBUTE_FACTORY;

        StandardTokenizer tokenizer = new StandardTokenizer(factory);
        tokenizer.setReader(new StringReader(document));
        tokenizer.reset();
        return tokenizer;
    }
    
    public String wordPorterStemming(String word){
        EnglishStemmer english = new EnglishStemmer();
        english.setCurrent(word);
        english.stem();
        return english.getCurrent();
    }

    public final void getStopWords(String pathStopWords) throws IOException{
        String stopWd = FileUtility.readFile(pathStopWords, Charset.defaultCharset());
        stopWords = FileUtility.readGraph(stopWd);
    }
}
