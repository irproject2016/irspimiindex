/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.uantwerp.iretrieval.spimi.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.HashMap;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/**
 *
 * @author gerito
 */
public abstract class FileUtility {
    
    public static List<String> getFilesFolder(String path){
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        List <String> namesList = new ArrayList<>();
        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                namesList.add(listOfFile.getName());
            } else if (listOfFile.isDirectory()) {
                System.out.println("Directory " + listOfFile.getName());
            }
        }
        return namesList;
    }
    
    public static void writeDocument(String documentPath,String content) throws UnsupportedEncodingException, IOException{
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(documentPath), "utf-8"))) {
        writer.write(content);
        }
    }
    
    public static void printCurrentTime(String data) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        System.out.println(data +" "+ sdf.format(cal.getTime()) );
    }
    
    public static String readFile(String path, Charset encoding)
            throws IOException 
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
    
    public static HashMap<String,Integer> readGraph(String file){
        HashMap<String,Integer> stopWords = new HashMap<>();
        if (file == null){
                file="";
        }
        BufferedReader reader = new BufferedReader(new StringReader(file));
        String[] lines = file.split("\r\n|\r|\n");
        for (String line : lines) {
            try {
                String node =  reader.readLine();
                stopWords.put(node, 0);
            } catch (IOException e) {
            }
        }
        return stopWords;
    }
    
    public static void writeLineByLine(String line,String pathFile){
//        try(PrintWriter output = new PrintWriter(new FileWriter(pathFile,true))) 
//        {
//            output.printf("", line);
//        } 
//        catch (Exception e) {}
        try(FileWriter fw = new FileWriter(pathFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(line);
            //more code
//            out.println("more text");
            //more code
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }
    
    public static JSONArray getJsonArrayFromFile(String path) throws IOException, ParseException{
        JSONParser parser = new JSONParser();
        File subBlock = new File(path);
        return (JSONArray) parser.parse(new FileReader(subBlock));
    }


    
}
