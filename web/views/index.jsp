<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" /> 
        <title>Search</title>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/irspimiindex">Search Page</a></li>                    
                    <li><a href="/irspimiindex/statistics">Search Statistics</a></li> 
                    <li><a href="/irspimiindex/processes">Administration</a></li>                     
                </ul>
              </div>
            </div>
          </nav>
        
        <!--Content-->
        <div class="container-fluid">
            
            <div class="row">
                <img src="${pageContext.request.contextPath}/resources/images/Search_World.jpg" class="img-responsive center-block">
                <div class="col-md-6 col-md-offset-3">
                    <div> 
                        <form action="./SearchServlet" method="POST">
                            <div class="row">
                                <!--<div class="col-md-5">-->
                                Compression
                                <input type="checkbox" data-toggle="toggle" name="toggle" id="toggle">
                                <input type="text" value="" name="searchText" class="form-control" required="required"/></br>
                                    <input type="submit" name="action" value="Search Parliament Documents" class="btn btn-primary center-block" />
                                <!--</div>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/toogle.js"></script>
        
    </body>
</html>
