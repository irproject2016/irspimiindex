<%-- 
    Document   : index.jsp
    Created on : Apr 15, 2016, 9:12:50 PM
    Author     : gerito
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />
        <title>Search</title>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/irspimiindex">Search Page</a></li>                    
                    <li><a href="/irspimiindex/statistics">Search Statistics</a></li> 
                    <li><a href="/irspimiindex/processes">Administration</a></li> 
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        <!--Content-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="well"> 
                        <form action="./processes" method="POST">
                            <div class="row">
                                <div class="col-md-5">
                                    Type of File:
                                    </br>
                                    <input type="radio" name="type" value="0" checked>Pre-process Files<br>
                                    <input type="radio" name="type" value="1">Create Documents id(just do it one before indexing)<br>
                                    <input type="radio" name="type" value="2">Create SPIMI Index<br>
                                    </br>
                                    <input type="submit" name="action" value="Run processes" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </br></br>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                <table class="table table-striped">                    
                    <thead>
                        <tr><th><td align="center"><b>Posting List Contruction</b></td></th></tr>
                        <tr>
                            <th>Total Process Time</th>
                            <th>Number of Blocks</th> 
                            <th>Disk Input Time</th>
                            <th>Disk Output Time</th>
                            <th>Memory Estimation Time (ms)</th>
                        </tr>
                    </thead>
                    <tbody
                        <tr>
                            <td><c:out value="${evalSpimi.getTotalTime()}" /></td>
                            <td><c:out value="${evalSpimi.getNumberOfResultingBlocks()}" /></td>
                            <td><c:out value="${evalSpimi.getInTime()}" /></td>
                            <td><c:out value="${evalSpimi.getOutTime()}" /></td>
                            <td><c:out value="${evalSpimi.getMemoryStimationTime()}" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </br></br>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                <table class="table table-striped">
                    <thead>
                        <tr><th><td align="center"><b>Merge SPIMI Blocks</b></td></th></tr>
                        <tr>
                            <th>Total Process Time</th>
                            <th>Number of Blocks</th> 
                            <th>Disk Input Time</th>
                            <th>Disk Output Time</th>
                            <th>Memory Estimation Time (ms)</th>
                        </tr>
                    </thead>
                    <tbody
                        <tr>
                            <td><c:out value="${evalMerge.getTotalTime()}" /></td>
                            <td><c:out value="${evalMerge.getNumberEntries()}" /></td>                            
                            <td><c:out value="${evalMerge.getInTime()}" /></td>
                            <td><c:out value="${evalMerge.getOutTime()}" /></td>
                            <td><c:out value="${evalMerge.getMemoryStimationTime()}" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </br></br>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                <table class="table table-striped">
                    <thead>
                        <tr><th><td align="center"><b>Compression Times</b></td></th></tr>
                        <tr>
                            <th>Total Process Time</th>
                            <th>Number of Blocks</th> 
                            <th>Disk Input Time</th>
                            <th>Disk Output Time</th>
                            <th>Memory Estimation Time (ms)</th>
                        </tr>
                    </thead>
                    <tbody
                        <tr>
                            <td><c:out value="${evalCompression.getTotalTime()}" /></td>
                            <td><c:out value="${evalCompression.getNumberOfResultingBlocks()}" /></td>
                            <td><c:out value="${evalCompression.getInTime()}" /></td>
                            <td><c:out value="${evalCompression.getOutTime()}" /></td>
                            <td><c:out value="${evalCompression.getMemoryStimationTime()}" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
