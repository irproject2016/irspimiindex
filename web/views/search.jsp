<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/irspimiindex">Search Page</a></li>                    
                    <li><a href="/irspimiindex/statistics">Search Statistics</a></li> 
                    <li><a href="/irspimiindex/processes">Administration</a></li>  
                </ul>
              </div>
            </div>
        </nav>
        
        <!--Content-->
        <div class="container-fluid">            
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div> 
                        <form action="./SearchServlet" method="POST" >
                            <div class="row">
                                Compression
                                <input type="checkbox" data-toggle="toggle" name="toggle" id="toggle" /></br>
                                <input type="text" value="${sessionScope.queryTerm}" name="searchText" required="required" class="form-control"/></br>
                                Number&nbsp;of&nbsp;results:&nbsp;<b>${sessionScope.numberResults}</b>&nbsp;in&nbsp;<b>${sessionScope.timeQuery}</b> &nbsp;seconds
                                <input type="submit" name="action" value="Search Parliament Documents" class="btn btn-primary center-block" />
                            </div>
                        </form>
                        
                    </div>
                </div>
                </br></br>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id Document</th>
                                <th>Name Document</th>
                                <th>Location on Disk</th>
                            </tr>
                        </thead>
                        <tbody
                            <c:forEach items="${documentResults}" var="document">
                                <tr>
                                    <td><c:out value="${document.getDocumentId()}" /></td>
                                    <td><c:out value="${document.getDocumentName()}" /></td>
                                    <td><c:out value="${document.getDocumentURL()}" /></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/toogle.js"></script>        
    </body>
</html>
