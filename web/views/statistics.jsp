<%-- 
    Document   : index.jsp
    Created on : Apr 15, 2016, 9:12:50 PM
    Author     : gerito
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />
        <title>Search</title>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/irspimiindex">Search Page</a></li>                    
                        <li><a href="/irspimiindex/statistics">Search Statistics</a></li> 
                        <li><a href="/irspimiindex/processes">Administration</a></li>  
                    </ul>
                </div>
            </div>
        </nav>
        <!--Content-->
        <div class="container-fluid">

            </br></br>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                <table class="table table-striped">                    
                    <thead>
                        <tr><th><td align="center"><b>Search Statistics</b></td></th></tr>
                        <tr>
                            <th>Compression</th>                            
                            <th>Time to Process (ms)</th> 
                            <th>Number Of Results</th>
                            <th>Disk Intput Time</th>
                            <th>Memory Estimation Time (ms)</th>
                            <th>Query</th>
                        </tr>
                    </thead>
                    <tbody
                        <c:forEach items="${resultList}" var="evalSearch">
                            <tr>
                                <td><c:out value="${evalSearch.isCompression()}" /></td>
                                <td><c:out value="${evalSearch.getTotalTime()}" /></td>
                                <td><c:out value="${evalSearch.getNumberOfReults()}" /></td>
                                <td><c:out value="${evalSearch.getInTime()}" /></td>
                                <td><c:out value="${evalSearch.getMemoryStimationTime()}" /></td>
                                <td><c:out value="${evalSearch.getQuery()}" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
